//import { APIGatewayProxyHandler } from 'aws-lambda';
import "regenerator-runtime/runtime.js";
import { CognitoServiceProvider } from "./auth/aws";
import { successResponse, errorResponse} from './service/responseBuilder';
import * as dbService from './service/dbService';
import connect, {sql} from '@databases/pg';
const DB = connect()
const SQL = sql

export const login: any = async(event, _context)=>{

  const params = {
    UserPoolId: process.env.BAMBU_COGNITO_USER_POOL_ID,
    ClientId: process.env.BAMBU_COGNITO_APP_CLIENT_ID,
    AuthFlow: 'ADMIN_USER_PASSWORD_AUTH',
    AuthParameters:{
      USERNAME:event.body.username,
      PASSWORD:event.body.password
    },
  };

  const auth = await CognitoServiceProvider.adminInitiateAuth(params).promise();

  var cookieString = `apiToken=${auth.AuthenticationResult.IdToken}; Max-Age=${auth.AuthenticationResult.ExpiresIn}; SameSite=None;`;

  return {
    statusCode: 200,
    body: auth,
    cookie:cookieString
  }

}

export const getProductById: any = async(event, _context)=>{
  try{
    const productId = event.path.id;
    let product = await dbService.getProductById(DB, SQL, productId);
    return successResponse({data:product });
  }
  catch(error){
    return errorResponse({message:error.message});
  }
}

export const getProducts: any = async(event , _context)=>{
  try{
    console.log(event.query);

    var products;

    if(event.query){
     const itemsPerPage = event.query.itemsPerPage ? event.query.itemsPerPage : undefined;
     const pageNumber   = event.query.pageNumber ? event.query.pageNumber : undefined;
     const showAssetClassBreakdown = event.query.showAssetClassBreakdown ? event.query.showAssetClassBreakdown : undefined;
     const showGeographicalBreakdown = event.query.showGeographicalBreakdown ? event.query.showGeographicalBreakdown : undefined;
     const showOnly = event.query.showOnly ? event.query.showOnly : undefined;
     const filterByCode = event.query.filterByCode ? event.query.filterByCode : undefined;
     const filterByNames = event.query.filterByNames ? event.query.filterByNames : undefined;
     products = await dbService.getProductsDetails(DB, SQL , itemsPerPage , pageNumber , showAssetClassBreakdown, showGeographicalBreakdown, showOnly, filterByCode, filterByNames);
     return successResponse({data:products });
    }

    products = await dbService.getProductsDetails(DB, SQL);

    return successResponse({data:products });

  }
  catch(error){
    return errorResponse({message:error.message});
  }
 }









