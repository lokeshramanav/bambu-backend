export const getProductById = async (db, sql, productId) => {
    let productList = await db.query(sql `SELECT * FROM products WHERE id=${productId}`);
    var productDetails = {};
    for (let product of productList) {
        productDetails.uuid = product.id;
        productDetails.code = product.code;
        productDetails.name = product.name;
        productDetails.assetClassBreakdown = await getAssetClassDetailsForProductId(db, sql, productId);
        productDetails.geographicalBreakdown = await getGeographicalDetailsForProductId(db, sql, productId);
    }
    return productDetails;
};
export const getProductsDetails = async (db, sql, itemsPerPage, pageNumber, showAssetClassBreakdown, showGeographicalBreakdown, showOnly, filterByCode, filterByNames) => {
    var products = {};
    var productList;
    var totalPageNumbers;
    if (itemsPerPage && pageNumber) {
        var limit = itemsPerPage;
        var offset = (pageNumber * itemsPerPage) - itemsPerPage;
        if (filterByCode && filterByNames) {
            var codeFilter = sql.__dangerous__rawValue("('" + filterByCode.join("','") + "')");
            var nameFilter = sql.__dangerous__rawValue("('" + filterByNames.join("','") + "')");
            productList = await db.query(sql `SELECT * FROM products WHERE code in ${codeFilter} and name in ${nameFilter} LIMIT ${limit} OFFSET ${offset};`);
            totalPageNumbers = await db.query(sql `SELECT count(*) FROM products WHERE code in ${codeFilter} and name in ${nameFilter};`);
        }
        else if (filterByCode && !filterByNames) {
            var codeFilter = sql.__dangerous__rawValue("('" + filterByCode.join("','") + "')");
            productList = await db.query(sql `SELECT * FROM products WHERE code in ${codeFilter} LIMIT ${limit} OFFSET ${offset};`);
            totalPageNumbers = await db.query(sql `SELECT count(*) FROM products WHERE code in ${codeFilter};`);
        }
        else if (!filterByCode && filterByNames) {
            var nameFilter = sql.__dangerous__rawValue("('" + filterByNames.join("','") + "')");
            productList = await db.query(sql `SELECT * FROM products WHERE name in ${nameFilter} LIMIT ${limit} OFFSET ${offset};`);
            totalPageNumbers = await db.query(sql `SELECT count(*) FROM products  WHERE name in ${nameFilter};`);
        }
        else {
            productList = await db.query(sql `SELECT * FROM products LIMIT ${limit} OFFSET ${offset};`);
            totalPageNumbers = await db.query(sql `SELECT count(*) FROM products;`);
        }
        let pagination = {};
        pagination.itemsPerPage = itemsPerPage;
        pagination.pageNumber = pageNumber;
        pagination.numberOfPages = Math.ceil(totalPageNumbers[0].count / itemsPerPage);
        products.pagination = pagination;
        products.items = await getProductDetailsFromProductList(db, sql, productList, showAssetClassBreakdown, showGeographicalBreakdown, showOnly);
        return products;
    }
    else {
        if (filterByCode && filterByNames) {
            var codeFilter = sql.__dangerous__rawValue("('" + filterByCode.join("','") + "')");
            var nameFilter = sql.__dangerous__rawValue("('" + filterByNames.join("','") + "')");
            productList = await db.query(sql `SELECT * FROM products WHERE code in ${codeFilter} and name in ${nameFilter} ;`);
        }
        else if (filterByCode && !filterByNames) {
            var codeFilter = sql.__dangerous__rawValue("('" + filterByCode.join("','") + "')");
            productList = await db.query(sql `SELECT * FROM products WHERE code in ${codeFilter} ;`);
        }
        else if (!filterByCode && filterByNames) {
            var nameFilter = sql.__dangerous__rawValue("('" + filterByNames.join("','") + "')");
            productList = await db.query(sql `SELECT * FROM products WHERE name in ${nameFilter} ;`);
        }
        else {
            productList = await db.query(sql `SELECT * FROM products ;`);
        }
        products.items = await getProductDetailsFromProductList(db, sql, productList, showAssetClassBreakdown, showGeographicalBreakdown, showOnly);
        return products;
    }
};
export const getProductDetailsFromProductList = async (db, sql, productList, showAssetClassBreakdown, showGeographicalBreakdown, showOnly) => {
    let productDetailsList = [];
    for (let product of productList) {
        var productObject = {};
        if (showOnly) {
            for (let item of showOnly) {
                productObject[item] = product[item];
                if (item == 'id' || item == 'uuid') {
                    productObject["uuid"] = product.id;
                }
            }
        }
        else {
            productObject.uuid = product.id;
            productObject.code = product.code;
            productObject.name = product.name;
        }
        if (showAssetClassBreakdown) {
            productObject.assetClassBreakdown = await getAssetClassDetailsForProductId(db, sql, product.id);
        }
        if (showGeographicalBreakdown) {
            productObject.geographicalBreakdown = await getGeographicalDetailsForProductId(db, sql, product.id);
        }
        productDetailsList.push(productObject);
    }
    return productDetailsList;
};
export const getAssetClassDetailsForProductId = async (db, sql, productId) => {
    let assetClassListForProductId = await db.query(sql `SELECT * FROM asset_classes WHERE product_id=${productId}`);
    let assetClassBreakdown = [];
    for (let assetClass of assetClassListForProductId) {
        let asset = {};
        asset.allocationDate = new Date(assetClass.allocation_date).toISOString().substr(0, 10);
        asset.allocationPercentage = assetClass.allocation_percentage;
        asset.name = assetClass.name;
        assetClassBreakdown.push(asset);
    }
    return assetClassBreakdown;
};
export const getGeographicalDetailsForProductId = async (db, sql, productId) => {
    let geographicalZonesListForProductId = await db.query(sql `SELECT * FROM geographical_zones WHERE product_id=${productId}`);
    let geographicalBreakdown = [];
    for (let geoGraphicalzone of geographicalZonesListForProductId) {
        let zone = {};
        zone.allocationDate = new Date(geoGraphicalzone.allocation_date).toISOString().substr(0, 10);
        zone.allocationPercentage = geoGraphicalzone.allocation_percentage;
        zone.name = geoGraphicalzone.name;
        geographicalBreakdown.push(zone);
    }
    return geographicalBreakdown;
};
//# sourceMappingURL=dbService.js.map