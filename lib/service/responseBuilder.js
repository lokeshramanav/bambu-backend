export const successResponse = (payload, statusCode = 200) => {
    return responseBuilder(statusCode, payload);
};
export const errorResponse = (payload, statusCode = 500) => {
    return responseBuilder(statusCode, payload);
};
export const responseBuilder = (statusCode, payload) => {
    let { message, data } = payload;
    if (message instanceof Error) {
        console.log(message);
        message = message.message;
    }
    return {
        statusCode,
        body: {
            message: message ? message : "OK",
            data
        }
    };
};
//# sourceMappingURL=responseBuilder.js.map