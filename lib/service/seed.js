import connect, { sql } from '@databases/pg';
const DB = connect();
const SQL = sql;
export const dbLoader = async () => {
    for (let i = 0; i < 1000; i++) {
        let productCode = generateRandom4CharCode();
        let price = Math.floor(generateRandom2DigitNumber());
        let product = await loadProductsTable(productCode, "Product_" + i.toString(), price);
        console.log(product);
        await loadAssetClassesTable(product[0].id);
        await loadGeographicalZonesTable(product[0].id);
    }
};
export const loadProductsTable = async (code, name, price) => {
    return (await DB.query(SQL `INSERT INTO products (code, name, price)
                               VALUES (${code}, ${name}, ${price})
                               RETURNING *;`));
};
export const loadAssetClassesTable = async (productId) => {
    for (let m = 0; m < 4; m++) {
        let assetClasses = ['fixed income', 'cash', 'equity', 'other'];
        let allocationPercentage = Math.floor(generateRandom2DigitNumber());
        let allocationDate = new Date();
        allocationDate.setDate(allocationDate.getDate() - m);
        await loadAssetClassesQuery(assetClasses[m], allocationPercentage, allocationDate, productId);
    }
};
export const loadAssetClassesQuery = async (name, allocationPercentage, allocationDate, product_id) => {
    return (await DB.query(SQL `INSERT INTO asset_classes (name, allocation_percentage, allocation_date, product_id)
                               VALUES (${name}, ${allocationPercentage}, ${allocationDate}, ${product_id})
                               RETURNING *;`));
};
export const loadGeographicalZonesTable = async (productId) => {
    for (let m = 0; m < 5; m++) {
        let geographicalZones = ['FRANCE', 'IRAQ', 'CANADA', 'INDIA', 'RUSSIA'];
        let allocationPercentage = Math.floor(generateRandom2DigitNumber());
        let allocationDate = new Date();
        allocationDate.setDate(allocationDate.getDate() - m);
        await loadGeographicalZonesQuery(geographicalZones[m], allocationPercentage, allocationDate, productId);
    }
};
export const loadGeographicalZonesQuery = async (name, allocationPercentage, allocationDate, product_id) => {
    return (await DB.query(SQL `INSERT INTO geographical_zones (name, allocation_percentage, allocation_date, product_id)
                               VALUES (${name}, ${allocationPercentage}, ${allocationDate}, ${product_id})
                               RETURNING *;`));
};
const generateRandom4CharCode = () => {
    var code = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (let i = 0; i < 4; i++) {
        code += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return code;
};
const generateRandom2DigitNumber = () => {
    var randomNum = Math.random().toFixed(2);
    return randomNum * 100;
};
//# sourceMappingURL=seed.js.map