import "regenerator-runtime/runtime.js";
import { CognitoServiceProvider } from "./auth/aws";
import { successResponse, errorResponse } from './service/responseBuilder';
import * as dbService from './service/dbService';
import connect, { sql } from '@databases/pg';
const DB = connect();
const SQL = sql;
export const login = async (event, _context) => {
    const params = {
        UserPoolId: process.env.BAMBU_COGNITO_USER_POOL_ID,
        ClientId: process.env.BAMBU_COGNITO_APP_CLIENT_ID,
        AuthFlow: 'ADMIN_USER_PASSWORD_AUTH',
        AuthParameters: {
            USERNAME: event.body.username,
            PASSWORD: event.body.password
        },
    };
    const auth = await CognitoServiceProvider.adminInitiateAuth(params).promise();
    var cookieString = `apiToken=${auth.AuthenticationResult.IdToken}; Max-Age=${auth.AuthenticationResult.ExpiresIn}; SameSite=None;`;
    return {
        statusCode: 200,
        body: auth,
        cookie: cookieString
    };
};
export const getProductById = async (event, _context) => {
    try {
        const productId = event.path.id;
        let product = await dbService.getProductById(DB, SQL, productId);
        return successResponse({ data: product });
    }
    catch (error) {
        return errorResponse({ message: error.message });
    }
};
export const getProducts = async (event, _context) => {
    try {
        var products;
        if (event.query) {
            const itemsPerPage = event.query.itemsPerPage ? event.query.itemsPerPage : undefined;
            const pageNumber = event.query.pageNumber ? event.query.pageNumber : undefined;
            const showAssetClassBreakdown = event.query.showAssetClassBreakdown ? event.query.showAssetClassBreakdown : undefined;
            const showGeographicalBreakdown = event.query.showGeographicalBreakdown ? event.query.showGeographicalBreakdown : undefined;
            const showOnly = event.query.showOnly ? event.query.showOnly : undefined;
            const filterByCode = event.query.filterByCode ? event.query.filterByCode : undefined;
            const filterByNames = event.query.filterByNames ? event.query.filterByNames : undefined;
            products = await dbService.getProductsDetails(DB, SQL, itemsPerPage, pageNumber, showAssetClassBreakdown, showGeographicalBreakdown, showOnly, filterByCode, filterByNames);
            return successResponse({ data: products });
        }
        products = await dbService.getProductsDetails(DB, SQL);
        return successResponse({ data: products });
    }
    catch (error) {
        return errorResponse({ message: error.message });
    }
};
export const test = async (event, _context) => {
    console.log(event);
    await dbLoader();
};
export const dbLoader = async () => {
    for (let i = 0; i < 25000; i++) {
        let productCode = generateRandom4CharCode();
        let price = Math.floor(generateRandom2DigitNumber());
        let product = await loadProductsTable(productCode, "Product_" + i.toString(), price);
        console.log(product);
        await loadAssetClassesTable(product[0].id);
        await loadGeographicalZonesTable(product[0].id);
    }
};
export const loadProductsTable = async (code, name, price) => {
    return (await DB.query(SQL `INSERT INTO products (code, name, price)
                             VALUES (${code}, ${name}, ${price})
                             RETURNING *;`));
};
export const loadAssetClassesTable = async (productId) => {
    for (let m = 0; m < 4; m++) {
        let assetClasses = ['fixed income', 'cash', 'equity', 'other'];
        let allocationPercentage = Math.floor(generateRandom2DigitNumber());
        let allocationDate = new Date();
        allocationDate.setDate(allocationDate.getDate() - m);
        await loadAssetClassesQuery(assetClasses[m], allocationPercentage, allocationDate, productId);
    }
};
export const loadAssetClassesQuery = async (name, allocationPercentage, allocationDate, product_id) => {
    return (await DB.query(SQL `INSERT INTO asset_classes (name, allocation_percentage, allocation_date, product_id)
                             VALUES (${name}, ${allocationPercentage}, ${allocationDate}, ${product_id})
                             RETURNING *;`));
};
export const loadGeographicalZonesTable = async (productId) => {
    for (let m = 0; m < 5; m++) {
        let geographicalZones = ['FRANCE', 'IRAQ', 'CANADA', 'INDIA', 'RUSSIA'];
        let allocationPercentage = Math.floor(generateRandom2DigitNumber());
        let allocationDate = new Date();
        allocationDate.setDate(allocationDate.getDate() - m);
        await loadGeographicalZonesQuery(geographicalZones[m], allocationPercentage, allocationDate, productId);
    }
};
export const loadGeographicalZonesQuery = async (name, allocationPercentage, allocationDate, product_id) => {
    return (await DB.query(SQL `INSERT INTO geographical_zones (name, allocation_percentage, allocation_date, product_id)
                             VALUES (${name}, ${allocationPercentage}, ${allocationDate}, ${product_id})
                             RETURNING *;`));
};
const generateRandom4CharCode = () => {
    var code = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (let i = 0; i < 4; i++) {
        code += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return code;
};
const generateRandom2DigitNumber = () => {
    var randomNum = Math.random().toFixed(2);
    return randomNum * 100;
};
//# sourceMappingURL=handler.js.map