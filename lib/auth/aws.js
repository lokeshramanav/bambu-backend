import * as AWS from "aws-sdk";
export const config = {
    region: process.env.BAMBU_AWS_REGION,
    apiVersion: process.env.BAMBU_AWS_API_VERSION,
    accessKeyId: process.env.BAMBU_AWS_ID,
    secretAccessKey: process.env.BAMBU_AWS_SECRET
};
export const CognitoServiceProvider = new AWS.CognitoIdentityServiceProvider(config);
//# sourceMappingURL=aws.js.map