export const successResponse = (payload: any, statusCode: number = 200) => {
    return responseBuilder(statusCode, payload);
  };

export const errorResponse = (payload: any, statusCode: number = 500) => {
    return responseBuilder(statusCode, payload);
  };

export const responseBuilder = (statusCode: number, payload: any) => {
    let { message, data } = payload;
    if ( message instanceof Error) {
      console.log(message)
      message = message.message;
    }
    return {
      statusCode,
      body: {
        message: message ? message : "OK",
        data
      }
    };
  };
