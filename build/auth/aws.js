"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.CognitoServiceProvider = exports.config = undefined;

var _awsSdk = require("aws-sdk");

var AWS = _interopRequireWildcard(_awsSdk);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var config = exports.config = {
    region: process.env.BAMBU_AWS_REGION,
    apiVersion: process.env.BAMBU_AWS_API_VERSION,
    accessKeyId: process.env.BAMBU_AWS_ID,
    secretAccessKey: process.env.BAMBU_AWS_SECRET
};
var CognitoServiceProvider = exports.CognitoServiceProvider = new AWS.CognitoIdentityServiceProvider(config);
//# sourceMappingURL=aws.js.map