"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.loadGeographicalZonesQuery = exports.loadGeographicalZonesTable = exports.loadAssetClassesQuery = exports.loadAssetClassesTable = exports.loadProductsTable = exports.dbLoader = exports.test = exports.getProducts = exports.getProductById = exports.login = undefined;

var _templateObject = _taggedTemplateLiteral(["INSERT INTO products (code, name, price)\n                             VALUES (", ", ", ", ", ")\n                             RETURNING *;"], ["INSERT INTO products (code, name, price)\n                             VALUES (", ", ", ", ", ")\n                             RETURNING *;"]),
    _templateObject2 = _taggedTemplateLiteral(["INSERT INTO asset_classes (name, allocation_percentage, allocation_date, product_id)\n                             VALUES (", ", ", ", ", ", ", ")\n                             RETURNING *;"], ["INSERT INTO asset_classes (name, allocation_percentage, allocation_date, product_id)\n                             VALUES (", ", ", ", ", ", ", ")\n                             RETURNING *;"]),
    _templateObject3 = _taggedTemplateLiteral(["INSERT INTO geographical_zones (name, allocation_percentage, allocation_date, product_id)\n                             VALUES (", ", ", ", ", ", ", ")\n                             RETURNING *;"], ["INSERT INTO geographical_zones (name, allocation_percentage, allocation_date, product_id)\n                             VALUES (", ", ", ", ", ", ", ")\n                             RETURNING *;"]);

require("regenerator-runtime/runtime.js");

var _aws = require("./auth/aws");

var _responseBuilder = require("./service/responseBuilder");

var _dbService = require("./service/dbService");

var dbService = _interopRequireWildcard(_dbService);

var _pg = require("@databases/pg");

var _pg2 = _interopRequireDefault(_pg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var DB = (0, _pg2.default)();
var SQL = _pg.sql;
var login = exports.login = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(event, _context) {
        var params, auth, cookieString;
        return regeneratorRuntime.wrap(function _callee$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        params = {
                            UserPoolId: process.env.BAMBU_COGNITO_USER_POOL_ID,
                            ClientId: process.env.BAMBU_COGNITO_APP_CLIENT_ID,
                            AuthFlow: 'ADMIN_USER_PASSWORD_AUTH',
                            AuthParameters: {
                                USERNAME: event.body.username,
                                PASSWORD: event.body.password
                            }
                        };
                        _context2.next = 3;
                        return _aws.CognitoServiceProvider.adminInitiateAuth(params).promise();

                    case 3:
                        auth = _context2.sent;
                        cookieString = "apiToken=" + auth.AuthenticationResult.IdToken + "; Max-Age=" + auth.AuthenticationResult.ExpiresIn + "; SameSite=None;";
                        return _context2.abrupt("return", {
                            statusCode: 200,
                            body: auth,
                            cookie: cookieString
                        });

                    case 6:
                    case "end":
                        return _context2.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function login(_x, _x2) {
        return _ref.apply(this, arguments);
    };
}();
var getProductById = exports.getProductById = function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(event, _context) {
        var productId, product;
        return regeneratorRuntime.wrap(function _callee2$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _context3.prev = 0;
                        productId = event.path.id;
                        _context3.next = 4;
                        return dbService.getProductById(DB, SQL, productId);

                    case 4:
                        product = _context3.sent;
                        return _context3.abrupt("return", (0, _responseBuilder.successResponse)({ data: product }));

                    case 8:
                        _context3.prev = 8;
                        _context3.t0 = _context3["catch"](0);
                        return _context3.abrupt("return", (0, _responseBuilder.errorResponse)({ message: _context3.t0.message }));

                    case 11:
                    case "end":
                        return _context3.stop();
                }
            }
        }, _callee2, undefined, [[0, 8]]);
    }));

    return function getProductById(_x3, _x4) {
        return _ref2.apply(this, arguments);
    };
}();
var getProducts = exports.getProducts = function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(event, _context) {
        var products, itemsPerPage, pageNumber, showAssetClassBreakdown, showGeographicalBreakdown, showOnly, filterByCode, filterByNames;
        return regeneratorRuntime.wrap(function _callee3$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.prev = 0;

                        if (!event.query) {
                            _context4.next = 13;
                            break;
                        }

                        itemsPerPage = event.query.itemsPerPage ? event.query.itemsPerPage : undefined;
                        pageNumber = event.query.pageNumber ? event.query.pageNumber : undefined;
                        showAssetClassBreakdown = event.query.showAssetClassBreakdown ? event.query.showAssetClassBreakdown : undefined;
                        showGeographicalBreakdown = event.query.showGeographicalBreakdown ? event.query.showGeographicalBreakdown : undefined;
                        showOnly = event.query.showOnly ? event.query.showOnly : undefined;
                        filterByCode = event.query.filterByCode ? event.query.filterByCode : undefined;
                        filterByNames = event.query.filterByNames ? event.query.filterByNames : undefined;
                        _context4.next = 11;
                        return dbService.getProductsDetails(DB, SQL, itemsPerPage, pageNumber, showAssetClassBreakdown, showGeographicalBreakdown, showOnly, filterByCode, filterByNames);

                    case 11:
                        products = _context4.sent;
                        return _context4.abrupt("return", (0, _responseBuilder.successResponse)({ data: products }));

                    case 13:
                        _context4.next = 15;
                        return dbService.getProductsDetails(DB, SQL);

                    case 15:
                        products = _context4.sent;
                        return _context4.abrupt("return", (0, _responseBuilder.successResponse)({ data: products }));

                    case 19:
                        _context4.prev = 19;
                        _context4.t0 = _context4["catch"](0);
                        return _context4.abrupt("return", (0, _responseBuilder.errorResponse)({ message: _context4.t0.message }));

                    case 22:
                    case "end":
                        return _context4.stop();
                }
            }
        }, _callee3, undefined, [[0, 19]]);
    }));

    return function getProducts(_x5, _x6) {
        return _ref3.apply(this, arguments);
    };
}();
var test = exports.test = function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(event, _context) {
        return regeneratorRuntime.wrap(function _callee4$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        console.log(event);
                        _context5.next = 3;
                        return dbLoader();

                    case 3:
                    case "end":
                        return _context5.stop();
                }
            }
        }, _callee4, undefined);
    }));

    return function test(_x7, _x8) {
        return _ref4.apply(this, arguments);
    };
}();
var dbLoader = exports.dbLoader = function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
        var i, productCode, price, product;
        return regeneratorRuntime.wrap(function _callee5$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        i = 0;

                    case 1:
                        if (!(i < 25000)) {
                            _context6.next = 15;
                            break;
                        }

                        productCode = generateRandom4CharCode();
                        price = Math.floor(generateRandom2DigitNumber());
                        _context6.next = 6;
                        return loadProductsTable(productCode, "Product_" + i.toString(), price);

                    case 6:
                        product = _context6.sent;

                        console.log(product);
                        _context6.next = 10;
                        return loadAssetClassesTable(product[0].id);

                    case 10:
                        _context6.next = 12;
                        return loadGeographicalZonesTable(product[0].id);

                    case 12:
                        i++;
                        _context6.next = 1;
                        break;

                    case 15:
                    case "end":
                        return _context6.stop();
                }
            }
        }, _callee5, undefined);
    }));

    return function dbLoader() {
        return _ref5.apply(this, arguments);
    };
}();
var loadProductsTable = exports.loadProductsTable = function () {
    var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(code, name, price) {
        return regeneratorRuntime.wrap(function _callee6$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:
                        _context7.next = 2;
                        return DB.query(SQL(_templateObject, code, name, price));

                    case 2:
                        return _context7.abrupt("return", _context7.sent);

                    case 3:
                    case "end":
                        return _context7.stop();
                }
            }
        }, _callee6, undefined);
    }));

    return function loadProductsTable(_x9, _x10, _x11) {
        return _ref6.apply(this, arguments);
    };
}();
var loadAssetClassesTable = exports.loadAssetClassesTable = function () {
    var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(productId) {
        var m, assetClasses, allocationPercentage, allocationDate;
        return regeneratorRuntime.wrap(function _callee7$(_context8) {
            while (1) {
                switch (_context8.prev = _context8.next) {
                    case 0:
                        m = 0;

                    case 1:
                        if (!(m < 4)) {
                            _context8.next = 11;
                            break;
                        }

                        assetClasses = ['fixed income', 'cash', 'equity', 'other'];
                        allocationPercentage = Math.floor(generateRandom2DigitNumber());
                        allocationDate = new Date();

                        allocationDate.setDate(allocationDate.getDate() - m);
                        _context8.next = 8;
                        return loadAssetClassesQuery(assetClasses[m], allocationPercentage, allocationDate, productId);

                    case 8:
                        m++;
                        _context8.next = 1;
                        break;

                    case 11:
                    case "end":
                        return _context8.stop();
                }
            }
        }, _callee7, undefined);
    }));

    return function loadAssetClassesTable(_x12) {
        return _ref7.apply(this, arguments);
    };
}();
var loadAssetClassesQuery = exports.loadAssetClassesQuery = function () {
    var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(name, allocationPercentage, allocationDate, product_id) {
        return regeneratorRuntime.wrap(function _callee8$(_context9) {
            while (1) {
                switch (_context9.prev = _context9.next) {
                    case 0:
                        _context9.next = 2;
                        return DB.query(SQL(_templateObject2, name, allocationPercentage, allocationDate, product_id));

                    case 2:
                        return _context9.abrupt("return", _context9.sent);

                    case 3:
                    case "end":
                        return _context9.stop();
                }
            }
        }, _callee8, undefined);
    }));

    return function loadAssetClassesQuery(_x13, _x14, _x15, _x16) {
        return _ref8.apply(this, arguments);
    };
}();
var loadGeographicalZonesTable = exports.loadGeographicalZonesTable = function () {
    var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(productId) {
        var m, geographicalZones, allocationPercentage, allocationDate;
        return regeneratorRuntime.wrap(function _callee9$(_context10) {
            while (1) {
                switch (_context10.prev = _context10.next) {
                    case 0:
                        m = 0;

                    case 1:
                        if (!(m < 5)) {
                            _context10.next = 11;
                            break;
                        }

                        geographicalZones = ['FRANCE', 'IRAQ', 'CANADA', 'INDIA', 'RUSSIA'];
                        allocationPercentage = Math.floor(generateRandom2DigitNumber());
                        allocationDate = new Date();

                        allocationDate.setDate(allocationDate.getDate() - m);
                        _context10.next = 8;
                        return loadGeographicalZonesQuery(geographicalZones[m], allocationPercentage, allocationDate, productId);

                    case 8:
                        m++;
                        _context10.next = 1;
                        break;

                    case 11:
                    case "end":
                        return _context10.stop();
                }
            }
        }, _callee9, undefined);
    }));

    return function loadGeographicalZonesTable(_x17) {
        return _ref9.apply(this, arguments);
    };
}();
var loadGeographicalZonesQuery = exports.loadGeographicalZonesQuery = function () {
    var _ref10 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(name, allocationPercentage, allocationDate, product_id) {
        return regeneratorRuntime.wrap(function _callee10$(_context11) {
            while (1) {
                switch (_context11.prev = _context11.next) {
                    case 0:
                        _context11.next = 2;
                        return DB.query(SQL(_templateObject3, name, allocationPercentage, allocationDate, product_id));

                    case 2:
                        return _context11.abrupt("return", _context11.sent);

                    case 3:
                    case "end":
                        return _context11.stop();
                }
            }
        }, _callee10, undefined);
    }));

    return function loadGeographicalZonesQuery(_x18, _x19, _x20, _x21) {
        return _ref10.apply(this, arguments);
    };
}();
var generateRandom4CharCode = function generateRandom4CharCode() {
    var code = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (var i = 0; i < 4; i++) {
        code += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return code;
};
var generateRandom2DigitNumber = function generateRandom2DigitNumber() {
    var randomNum = Math.random().toFixed(2);
    return randomNum * 100;
};
//# sourceMappingURL=handler.js.map