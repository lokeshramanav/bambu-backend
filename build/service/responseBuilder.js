"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var successResponse = exports.successResponse = function successResponse(payload) {
    var statusCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 200;

    return responseBuilder(statusCode, payload);
};
var errorResponse = exports.errorResponse = function errorResponse(payload) {
    var statusCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;

    return responseBuilder(statusCode, payload);
};
var responseBuilder = exports.responseBuilder = function responseBuilder(statusCode, payload) {
    var message = payload.message,
        data = payload.data;

    if (message instanceof Error) {
        console.log(message);
        message = message.message;
    }
    return {
        statusCode: statusCode,
        body: {
            message: message ? message : "OK",
            data: data
        }
    };
};
//# sourceMappingURL=responseBuilder.js.map