"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _templateObject = _taggedTemplateLiteral(["SELECT * FROM products WHERE id=", ""], ["SELECT * FROM products WHERE id=", ""]),
    _templateObject2 = _taggedTemplateLiteral(["SELECT * FROM products WHERE code in ", " and name in ", " LIMIT ", " OFFSET ", ";"], ["SELECT * FROM products WHERE code in ", " and name in ", " LIMIT ", " OFFSET ", ";"]),
    _templateObject3 = _taggedTemplateLiteral(["SELECT count(*) FROM products WHERE code in ", " and name in ", ";"], ["SELECT count(*) FROM products WHERE code in ", " and name in ", ";"]),
    _templateObject4 = _taggedTemplateLiteral(["SELECT * FROM products WHERE code in ", " LIMIT ", " OFFSET ", ";"], ["SELECT * FROM products WHERE code in ", " LIMIT ", " OFFSET ", ";"]),
    _templateObject5 = _taggedTemplateLiteral(["SELECT count(*) FROM products WHERE code in ", ";"], ["SELECT count(*) FROM products WHERE code in ", ";"]),
    _templateObject6 = _taggedTemplateLiteral(["SELECT * FROM products WHERE name in ", " LIMIT ", " OFFSET ", ";"], ["SELECT * FROM products WHERE name in ", " LIMIT ", " OFFSET ", ";"]),
    _templateObject7 = _taggedTemplateLiteral(["SELECT count(*) FROM products  WHERE name in ", ";"], ["SELECT count(*) FROM products  WHERE name in ", ";"]),
    _templateObject8 = _taggedTemplateLiteral(["SELECT * FROM products LIMIT ", " OFFSET ", ";"], ["SELECT * FROM products LIMIT ", " OFFSET ", ";"]),
    _templateObject9 = _taggedTemplateLiteral(["SELECT count(*) FROM products;"], ["SELECT count(*) FROM products;"]),
    _templateObject10 = _taggedTemplateLiteral(["SELECT * FROM products WHERE code in ", " and name in ", " ;"], ["SELECT * FROM products WHERE code in ", " and name in ", " ;"]),
    _templateObject11 = _taggedTemplateLiteral(["SELECT * FROM products WHERE code in ", " ;"], ["SELECT * FROM products WHERE code in ", " ;"]),
    _templateObject12 = _taggedTemplateLiteral(["SELECT * FROM products WHERE name in ", " ;"], ["SELECT * FROM products WHERE name in ", " ;"]),
    _templateObject13 = _taggedTemplateLiteral(["SELECT * FROM products ;"], ["SELECT * FROM products ;"]),
    _templateObject14 = _taggedTemplateLiteral(["SELECT * FROM asset_classes WHERE product_id=", ""], ["SELECT * FROM asset_classes WHERE product_id=", ""]),
    _templateObject15 = _taggedTemplateLiteral(["SELECT * FROM geographical_zones WHERE product_id=", ""], ["SELECT * FROM geographical_zones WHERE product_id=", ""]);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var getProductById = exports.getProductById = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(db, sql, productId) {
        var productList, productDetails, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, product;

        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.next = 2;
                        return db.query(sql(_templateObject, productId));

                    case 2:
                        productList = _context.sent;
                        productDetails = {};
                        _iteratorNormalCompletion = true;
                        _didIteratorError = false;
                        _iteratorError = undefined;
                        _context.prev = 7;
                        _iterator = productList[Symbol.iterator]();

                    case 9:
                        if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                            _context.next = 23;
                            break;
                        }

                        product = _step.value;

                        productDetails.uuid = product.id;
                        productDetails.code = product.code;
                        productDetails.name = product.name;
                        _context.next = 16;
                        return getAssetClassDetailsForProductId(db, sql, productId);

                    case 16:
                        productDetails.assetClassBreakdown = _context.sent;
                        _context.next = 19;
                        return getGeographicalDetailsForProductId(db, sql, productId);

                    case 19:
                        productDetails.geographicalBreakdown = _context.sent;

                    case 20:
                        _iteratorNormalCompletion = true;
                        _context.next = 9;
                        break;

                    case 23:
                        _context.next = 29;
                        break;

                    case 25:
                        _context.prev = 25;
                        _context.t0 = _context["catch"](7);
                        _didIteratorError = true;
                        _iteratorError = _context.t0;

                    case 29:
                        _context.prev = 29;
                        _context.prev = 30;

                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }

                    case 32:
                        _context.prev = 32;

                        if (!_didIteratorError) {
                            _context.next = 35;
                            break;
                        }

                        throw _iteratorError;

                    case 35:
                        return _context.finish(32);

                    case 36:
                        return _context.finish(29);

                    case 37:
                        return _context.abrupt("return", productDetails);

                    case 38:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, undefined, [[7, 25, 29, 37], [30,, 32, 36]]);
    }));

    return function getProductById(_x, _x2, _x3) {
        return _ref.apply(this, arguments);
    };
}();
var getProductsDetails = exports.getProductsDetails = function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(db, sql, itemsPerPage, pageNumber, showAssetClassBreakdown, showGeographicalBreakdown, showOnly, filterByCode, filterByNames) {
        var products, productList, totalPageNumbers, limit, offset, codeFilter, nameFilter, pagination;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        products = {};

                        if (!(itemsPerPage && pageNumber)) {
                            _context2.next = 52;
                            break;
                        }

                        limit = itemsPerPage;
                        offset = pageNumber * itemsPerPage - itemsPerPage;

                        if (!(filterByCode && filterByNames)) {
                            _context2.next = 15;
                            break;
                        }

                        codeFilter = sql.__dangerous__rawValue("('" + filterByCode.join("','") + "')");
                        nameFilter = sql.__dangerous__rawValue("('" + filterByNames.join("','") + "')");
                        _context2.next = 9;
                        return db.query(sql(_templateObject2, codeFilter, nameFilter, limit, offset));

                    case 9:
                        productList = _context2.sent;
                        _context2.next = 12;
                        return db.query(sql(_templateObject3, codeFilter, nameFilter));

                    case 12:
                        totalPageNumbers = _context2.sent;
                        _context2.next = 41;
                        break;

                    case 15:
                        if (!(filterByCode && !filterByNames)) {
                            _context2.next = 25;
                            break;
                        }

                        codeFilter = sql.__dangerous__rawValue("('" + filterByCode.join("','") + "')");
                        _context2.next = 19;
                        return db.query(sql(_templateObject4, codeFilter, limit, offset));

                    case 19:
                        productList = _context2.sent;
                        _context2.next = 22;
                        return db.query(sql(_templateObject5, codeFilter));

                    case 22:
                        totalPageNumbers = _context2.sent;
                        _context2.next = 41;
                        break;

                    case 25:
                        if (!(!filterByCode && filterByNames)) {
                            _context2.next = 35;
                            break;
                        }

                        nameFilter = sql.__dangerous__rawValue("('" + filterByNames.join("','") + "')");
                        _context2.next = 29;
                        return db.query(sql(_templateObject6, nameFilter, limit, offset));

                    case 29:
                        productList = _context2.sent;
                        _context2.next = 32;
                        return db.query(sql(_templateObject7, nameFilter));

                    case 32:
                        totalPageNumbers = _context2.sent;
                        _context2.next = 41;
                        break;

                    case 35:
                        _context2.next = 37;
                        return db.query(sql(_templateObject8, limit, offset));

                    case 37:
                        productList = _context2.sent;
                        _context2.next = 40;
                        return db.query(sql(_templateObject9));

                    case 40:
                        totalPageNumbers = _context2.sent;

                    case 41:
                        pagination = {};

                        pagination.itemsPerPage = itemsPerPage;
                        pagination.pageNumber = pageNumber;
                        pagination.numberOfPages = Math.ceil(totalPageNumbers[0].count / itemsPerPage);
                        products.pagination = pagination;
                        _context2.next = 48;
                        return getProductDetailsFromProductList(db, sql, productList, showAssetClassBreakdown, showGeographicalBreakdown, showOnly);

                    case 48:
                        products.items = _context2.sent;
                        return _context2.abrupt("return", products);

                    case 52:
                        if (!(filterByCode && filterByNames)) {
                            _context2.next = 60;
                            break;
                        }

                        codeFilter = sql.__dangerous__rawValue("('" + filterByCode.join("','") + "')");
                        nameFilter = sql.__dangerous__rawValue("('" + filterByNames.join("','") + "')");
                        _context2.next = 57;
                        return db.query(sql(_templateObject10, codeFilter, nameFilter));

                    case 57:
                        productList = _context2.sent;
                        _context2.next = 77;
                        break;

                    case 60:
                        if (!(filterByCode && !filterByNames)) {
                            _context2.next = 67;
                            break;
                        }

                        codeFilter = sql.__dangerous__rawValue("('" + filterByCode.join("','") + "')");
                        _context2.next = 64;
                        return db.query(sql(_templateObject11, codeFilter));

                    case 64:
                        productList = _context2.sent;
                        _context2.next = 77;
                        break;

                    case 67:
                        if (!(!filterByCode && filterByNames)) {
                            _context2.next = 74;
                            break;
                        }

                        nameFilter = sql.__dangerous__rawValue("('" + filterByNames.join("','") + "')");
                        _context2.next = 71;
                        return db.query(sql(_templateObject12, nameFilter));

                    case 71:
                        productList = _context2.sent;
                        _context2.next = 77;
                        break;

                    case 74:
                        _context2.next = 76;
                        return db.query(sql(_templateObject13));

                    case 76:
                        productList = _context2.sent;

                    case 77:
                        _context2.next = 79;
                        return getProductDetailsFromProductList(db, sql, productList, showAssetClassBreakdown, showGeographicalBreakdown, showOnly);

                    case 79:
                        products.items = _context2.sent;
                        return _context2.abrupt("return", products);

                    case 81:
                    case "end":
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    }));

    return function getProductsDetails(_x4, _x5, _x6, _x7, _x8, _x9, _x10, _x11, _x12) {
        return _ref2.apply(this, arguments);
    };
}();
var getProductDetailsFromProductList = exports.getProductDetailsFromProductList = function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(db, sql, productList, showAssetClassBreakdown, showGeographicalBreakdown, showOnly) {
        var productDetailsList, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, product, productObject, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, item;

        return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        productDetailsList = [];
                        _iteratorNormalCompletion2 = true;
                        _didIteratorError2 = false;
                        _iteratorError2 = undefined;
                        _context3.prev = 4;
                        _iterator2 = productList[Symbol.iterator]();

                    case 6:
                        if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                            _context3.next = 46;
                            break;
                        }

                        product = _step2.value;
                        productObject = {};

                        if (!showOnly) {
                            _context3.next = 31;
                            break;
                        }

                        _iteratorNormalCompletion3 = true;
                        _didIteratorError3 = false;
                        _iteratorError3 = undefined;
                        _context3.prev = 13;

                        for (_iterator3 = showOnly[Symbol.iterator](); !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                            item = _step3.value;

                            productObject[item] = product[item];
                            if (item == 'id' || item == 'uuid') {
                                productObject["uuid"] = product.id;
                            }
                        }
                        _context3.next = 21;
                        break;

                    case 17:
                        _context3.prev = 17;
                        _context3.t0 = _context3["catch"](13);
                        _didIteratorError3 = true;
                        _iteratorError3 = _context3.t0;

                    case 21:
                        _context3.prev = 21;
                        _context3.prev = 22;

                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                            _iterator3.return();
                        }

                    case 24:
                        _context3.prev = 24;

                        if (!_didIteratorError3) {
                            _context3.next = 27;
                            break;
                        }

                        throw _iteratorError3;

                    case 27:
                        return _context3.finish(24);

                    case 28:
                        return _context3.finish(21);

                    case 29:
                        _context3.next = 34;
                        break;

                    case 31:
                        productObject.uuid = product.id;
                        productObject.code = product.code;
                        productObject.name = product.name;

                    case 34:
                        if (!showAssetClassBreakdown) {
                            _context3.next = 38;
                            break;
                        }

                        _context3.next = 37;
                        return getAssetClassDetailsForProductId(db, sql, product.id);

                    case 37:
                        productObject.assetClassBreakdown = _context3.sent;

                    case 38:
                        if (!showGeographicalBreakdown) {
                            _context3.next = 42;
                            break;
                        }

                        _context3.next = 41;
                        return getGeographicalDetailsForProductId(db, sql, product.id);

                    case 41:
                        productObject.geographicalBreakdown = _context3.sent;

                    case 42:
                        productDetailsList.push(productObject);

                    case 43:
                        _iteratorNormalCompletion2 = true;
                        _context3.next = 6;
                        break;

                    case 46:
                        _context3.next = 52;
                        break;

                    case 48:
                        _context3.prev = 48;
                        _context3.t1 = _context3["catch"](4);
                        _didIteratorError2 = true;
                        _iteratorError2 = _context3.t1;

                    case 52:
                        _context3.prev = 52;
                        _context3.prev = 53;

                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }

                    case 55:
                        _context3.prev = 55;

                        if (!_didIteratorError2) {
                            _context3.next = 58;
                            break;
                        }

                        throw _iteratorError2;

                    case 58:
                        return _context3.finish(55);

                    case 59:
                        return _context3.finish(52);

                    case 60:
                        return _context3.abrupt("return", productDetailsList);

                    case 61:
                    case "end":
                        return _context3.stop();
                }
            }
        }, _callee3, undefined, [[4, 48, 52, 60], [13, 17, 21, 29], [22,, 24, 28], [53,, 55, 59]]);
    }));

    return function getProductDetailsFromProductList(_x13, _x14, _x15, _x16, _x17, _x18) {
        return _ref3.apply(this, arguments);
    };
}();
var getAssetClassDetailsForProductId = exports.getAssetClassDetailsForProductId = function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(db, sql, productId) {
        var assetClassListForProductId, assetClassBreakdown, _iteratorNormalCompletion4, _didIteratorError4, _iteratorError4, _iterator4, _step4, assetClass, asset;

        return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.next = 2;
                        return db.query(sql(_templateObject14, productId));

                    case 2:
                        assetClassListForProductId = _context4.sent;
                        assetClassBreakdown = [];
                        _iteratorNormalCompletion4 = true;
                        _didIteratorError4 = false;
                        _iteratorError4 = undefined;
                        _context4.prev = 7;

                        for (_iterator4 = assetClassListForProductId[Symbol.iterator](); !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                            assetClass = _step4.value;
                            asset = {};

                            asset.allocationDate = new Date(assetClass.allocation_date).toISOString().substr(0, 10);
                            asset.allocationPercentage = assetClass.allocation_percentage;
                            asset.name = assetClass.name;
                            assetClassBreakdown.push(asset);
                        }
                        _context4.next = 15;
                        break;

                    case 11:
                        _context4.prev = 11;
                        _context4.t0 = _context4["catch"](7);
                        _didIteratorError4 = true;
                        _iteratorError4 = _context4.t0;

                    case 15:
                        _context4.prev = 15;
                        _context4.prev = 16;

                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }

                    case 18:
                        _context4.prev = 18;

                        if (!_didIteratorError4) {
                            _context4.next = 21;
                            break;
                        }

                        throw _iteratorError4;

                    case 21:
                        return _context4.finish(18);

                    case 22:
                        return _context4.finish(15);

                    case 23:
                        return _context4.abrupt("return", assetClassBreakdown);

                    case 24:
                    case "end":
                        return _context4.stop();
                }
            }
        }, _callee4, undefined, [[7, 11, 15, 23], [16,, 18, 22]]);
    }));

    return function getAssetClassDetailsForProductId(_x19, _x20, _x21) {
        return _ref4.apply(this, arguments);
    };
}();
var getGeographicalDetailsForProductId = exports.getGeographicalDetailsForProductId = function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(db, sql, productId) {
        var geographicalZonesListForProductId, geographicalBreakdown, _iteratorNormalCompletion5, _didIteratorError5, _iteratorError5, _iterator5, _step5, geoGraphicalzone, zone;

        return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        _context5.next = 2;
                        return db.query(sql(_templateObject15, productId));

                    case 2:
                        geographicalZonesListForProductId = _context5.sent;
                        geographicalBreakdown = [];
                        _iteratorNormalCompletion5 = true;
                        _didIteratorError5 = false;
                        _iteratorError5 = undefined;
                        _context5.prev = 7;

                        for (_iterator5 = geographicalZonesListForProductId[Symbol.iterator](); !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                            geoGraphicalzone = _step5.value;
                            zone = {};

                            zone.allocationDate = new Date(geoGraphicalzone.allocation_date).toISOString().substr(0, 10);
                            zone.allocationPercentage = geoGraphicalzone.allocation_percentage;
                            zone.name = geoGraphicalzone.name;
                            geographicalBreakdown.push(zone);
                        }
                        _context5.next = 15;
                        break;

                    case 11:
                        _context5.prev = 11;
                        _context5.t0 = _context5["catch"](7);
                        _didIteratorError5 = true;
                        _iteratorError5 = _context5.t0;

                    case 15:
                        _context5.prev = 15;
                        _context5.prev = 16;

                        if (!_iteratorNormalCompletion5 && _iterator5.return) {
                            _iterator5.return();
                        }

                    case 18:
                        _context5.prev = 18;

                        if (!_didIteratorError5) {
                            _context5.next = 21;
                            break;
                        }

                        throw _iteratorError5;

                    case 21:
                        return _context5.finish(18);

                    case 22:
                        return _context5.finish(15);

                    case 23:
                        return _context5.abrupt("return", geographicalBreakdown);

                    case 24:
                    case "end":
                        return _context5.stop();
                }
            }
        }, _callee5, undefined, [[7, 11, 15, 23], [16,, 18, 22]]);
    }));

    return function getGeographicalDetailsForProductId(_x22, _x23, _x24) {
        return _ref5.apply(this, arguments);
    };
}();
//# sourceMappingURL=dbService.js.map