'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.loadGeographicalZonesQuery = exports.loadGeographicalZonesTable = exports.loadAssetClassesQuery = exports.loadAssetClassesTable = exports.loadProductsTable = exports.dbLoader = undefined;

var _templateObject = _taggedTemplateLiteral(['INSERT INTO products (code, name, price)\n                               VALUES (', ', ', ', ', ')\n                               RETURNING *;'], ['INSERT INTO products (code, name, price)\n                               VALUES (', ', ', ', ', ')\n                               RETURNING *;']),
    _templateObject2 = _taggedTemplateLiteral(['INSERT INTO asset_classes (name, allocation_percentage, allocation_date, product_id)\n                               VALUES (', ', ', ', ', ', ', ')\n                               RETURNING *;'], ['INSERT INTO asset_classes (name, allocation_percentage, allocation_date, product_id)\n                               VALUES (', ', ', ', ', ', ', ')\n                               RETURNING *;']),
    _templateObject3 = _taggedTemplateLiteral(['INSERT INTO geographical_zones (name, allocation_percentage, allocation_date, product_id)\n                               VALUES (', ', ', ', ', ', ', ')\n                               RETURNING *;'], ['INSERT INTO geographical_zones (name, allocation_percentage, allocation_date, product_id)\n                               VALUES (', ', ', ', ', ', ', ')\n                               RETURNING *;']);

var _pg = require('@databases/pg');

var _pg2 = _interopRequireDefault(_pg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var DB = (0, _pg2.default)();
var SQL = _pg.sql;
var dbLoader = exports.dbLoader = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var i, productCode, price, product;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        i = 0;

                    case 1:
                        if (!(i < 1000)) {
                            _context.next = 15;
                            break;
                        }

                        productCode = generateRandom4CharCode();
                        price = Math.floor(generateRandom2DigitNumber());
                        _context.next = 6;
                        return loadProductsTable(productCode, "Product_" + i.toString(), price);

                    case 6:
                        product = _context.sent;

                        console.log(product);
                        _context.next = 10;
                        return loadAssetClassesTable(product[0].id);

                    case 10:
                        _context.next = 12;
                        return loadGeographicalZonesTable(product[0].id);

                    case 12:
                        i++;
                        _context.next = 1;
                        break;

                    case 15:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function dbLoader() {
        return _ref.apply(this, arguments);
    };
}();
var loadProductsTable = exports.loadProductsTable = function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(code, name, price) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        _context2.next = 2;
                        return DB.query(SQL(_templateObject, code, name, price));

                    case 2:
                        return _context2.abrupt('return', _context2.sent);

                    case 3:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    }));

    return function loadProductsTable(_x, _x2, _x3) {
        return _ref2.apply(this, arguments);
    };
}();
var loadAssetClassesTable = exports.loadAssetClassesTable = function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(productId) {
        var m, assetClasses, allocationPercentage, allocationDate;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        m = 0;

                    case 1:
                        if (!(m < 4)) {
                            _context3.next = 11;
                            break;
                        }

                        assetClasses = ['fixed income', 'cash', 'equity', 'other'];
                        allocationPercentage = Math.floor(generateRandom2DigitNumber());
                        allocationDate = new Date();

                        allocationDate.setDate(allocationDate.getDate() - m);
                        _context3.next = 8;
                        return loadAssetClassesQuery(assetClasses[m], allocationPercentage, allocationDate, productId);

                    case 8:
                        m++;
                        _context3.next = 1;
                        break;

                    case 11:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, undefined);
    }));

    return function loadAssetClassesTable(_x4) {
        return _ref3.apply(this, arguments);
    };
}();
var loadAssetClassesQuery = exports.loadAssetClassesQuery = function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(name, allocationPercentage, allocationDate, product_id) {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.next = 2;
                        return DB.query(SQL(_templateObject2, name, allocationPercentage, allocationDate, product_id));

                    case 2:
                        return _context4.abrupt('return', _context4.sent);

                    case 3:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, undefined);
    }));

    return function loadAssetClassesQuery(_x5, _x6, _x7, _x8) {
        return _ref4.apply(this, arguments);
    };
}();
var loadGeographicalZonesTable = exports.loadGeographicalZonesTable = function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(productId) {
        var m, geographicalZones, allocationPercentage, allocationDate;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        m = 0;

                    case 1:
                        if (!(m < 5)) {
                            _context5.next = 11;
                            break;
                        }

                        geographicalZones = ['FRANCE', 'IRAQ', 'CANADA', 'INDIA', 'RUSSIA'];
                        allocationPercentage = Math.floor(generateRandom2DigitNumber());
                        allocationDate = new Date();

                        allocationDate.setDate(allocationDate.getDate() - m);
                        _context5.next = 8;
                        return loadGeographicalZonesQuery(geographicalZones[m], allocationPercentage, allocationDate, productId);

                    case 8:
                        m++;
                        _context5.next = 1;
                        break;

                    case 11:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, undefined);
    }));

    return function loadGeographicalZonesTable(_x9) {
        return _ref5.apply(this, arguments);
    };
}();
var loadGeographicalZonesQuery = exports.loadGeographicalZonesQuery = function () {
    var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(name, allocationPercentage, allocationDate, product_id) {
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        _context6.next = 2;
                        return DB.query(SQL(_templateObject3, name, allocationPercentage, allocationDate, product_id));

                    case 2:
                        return _context6.abrupt('return', _context6.sent);

                    case 3:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, undefined);
    }));

    return function loadGeographicalZonesQuery(_x10, _x11, _x12, _x13) {
        return _ref6.apply(this, arguments);
    };
}();
var generateRandom4CharCode = function generateRandom4CharCode() {
    var code = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (var i = 0; i < 4; i++) {
        code += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return code;
};
var generateRandom2DigitNumber = function generateRandom2DigitNumber() {
    var randomNum = Math.random().toFixed(2);
    return randomNum * 100;
};
//# sourceMappingURL=seed.js.map